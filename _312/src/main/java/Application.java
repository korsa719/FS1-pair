import java.io.File;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class Application {

  public static void main(String[] args) throws Exception {
    Server server = new Server(8080);
    ServletContextHandler handler = new ServletContextHandler();
    Chat servlet  = new Chat();
    handler.addServlet(new ServletHolder(servlet), "/*");
    server.setHandler(handler);
    server.start();
    server.join();
  }
}